# -*- coding:utf-8 -*-
from odoo import http


class Books(http.Controller):
    """
    显示有效图书列表的简单网页
    访问地址：192.168.99.100:8069/mine/library/books
    """
    @http.route('/mine/library/books', auth='user')
    def list(self, **kwargs):
        Book = http.request.env['mine.library.book']
        books = Book.search([])
        return http.request.render(
            'lib_app.book_list_template', {'books': books})