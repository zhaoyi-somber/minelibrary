# -*- coding: utf-8 -*-
{
    'name': "Library Management",

    'summary': """
    Library Management：图书信息管理
    """,

    'description': """
        图书管理系统
    """,

    'author': "somber",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base'],

    # always loaded
    'data': [
        'security/library_security.xml',
        'security/ir.model.access.csv',
        'views/mine_library_book.xml',
        'views/library_menus.xml',
        'views/book_list_template.xml',
        'data/library_data.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
    ],
}