# -*- coding:utf-8 -*-

from odoo import api, fields, models


class MineBookCategory(models.Model):
    _name = 'mine.library.book.category'
    _description = 'Mine Book Category'
    _parent_store = True

    name = fields.Char(translate=True, required=True)
    # 父类别
    parent_id = fields.Many2one(comodel_name='mine.library.book.category', string='Parent Category', ondelete='restrict')
    parent_path = fields.Char(index=True)
    # 子类别
    child_ids = fields.One2many(comodel_name='mine.library.book.category', inverse_name='parent_id', string='Subcategories')
    # 分类突出显示字段
    highlighted_id = fields.Reference([('mine.library.book', 'Book'), ('res.partner', 'Author')], string='Category Highlight')