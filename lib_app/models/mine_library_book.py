# -*- coding:utf-8 -*-

from odoo import _, models, fields, api, exceptions


class MineLibraryBook(models.Model):
    """
    图书管理系统-图书
    """
    _name = 'mine.library.book'
    _description = 'Mine Library Book'
    _order = 'name, date_published desc'

    # 书名
    name = fields.Char(string='Book name', index=True, help='Book cover title',)
    # ISBN
    isbn = fields.Char(string='Book ISBN', help='Book ISBN')
    # 有效
    active = fields.Boolean(string='Active', help='Active', default=True)
    # 出版日期
    date_published = fields.Date(string='Date Published', help='Date Published')
    # 图片
    image = fields.Binary(string='Image', help='Image')
    # 出版公司
    publisher_id = fields.Many2one(comodel_name='res.partner', string='Publisher', help='Publisher')
    # 作者
    author_ids = fields.Many2many(comodel_name='res.partner', string='Authors', help='Authors')
    # 图书类型
    book_type = fields.Selection([('paper', 'Paperback'), ('hard', 'Hardcover'), ('electronic', 'Electronic'), ('other', 'Other')],
                                 string='Type')
    # 内部说明
    notes = fields.Text(string='Internal Notes')
    # 说明
    descr = fields.Html(string='Description')
    # 数量
    copies = fields.Integer(default=1, string='Copies')
    # 评分
    avg_rating = fields.Float(string='Average Rating', digits=(3, 2))
    # 单价
    price = fields.Monetary(string='Price', currency_field='currency_id')
    # 币种
    currency_id = fields.Many2one(comodel_name='res.currency', string='Currency')
    # 最后出借日期
    last_borrow_date = fields.Datetime(string='Last Borrowed On', default=lambda self: fields.Datetime.now())
    # 出版国家
    publisher_country_id = fields.Many2one(
        'res.country', string='Publisher Country',
        related='publisher_id.country_id', readonly=True
    )

    # 唯一性校验：标题和出版日期相同的图书
    # 检查出版日期是否为未出版
    _sql_constraints = [
        ('unique_name_date_published',
         'UNIQUE (name, date_published)',
         _('Book title and publication date must be unique')),
        ('library_book_check_date',
         'CHECK (date_published <= current_date)',
         _('Publication date must not be in the future.')),
    ]

    @api.constrains('isbn')
    def _constrain_isbn_valid(self):
        """
        校验ISBN
        :return:
        """
        for book in self:
            if book.isbn and not book._check_isbn():
                raise exceptions.ValidationError('%s is an invalid ISBN' % book.isbn)

    @api.multi
    def _check_isbn(self):
        """
        ISBN校验
        :return:
        """
        # 现代ISBN规定位数
        isbn_length = 13
        # 为保持兼容性自行添加
        isbn = self.isbn.replace('-', '')
        digits = [int(x) for x in isbn if x.isdigit()]
        # 如果ISBN位数正确，进行校验
        if len(digits) == isbn_length:
            ponderations = [1, 3] * 6
            terms = [a * b for a, b in zip(digits[:12], ponderations)]
            remain = sum(terms) % 10
            check = 10 - remain if remain != 0 else 0
            return digits[-1] == check

    @api.multi
    def button_check_isbn(self):
        """
        按钮：校验ISBN 执行逻辑
        :return:
        """
        for book in self:
            if book.isbn:
                if not book._check_isbn():
                    raise exceptions.ValidationError(_('%s is an invalid ISBN') % book.isbn)
            else:
                raise exceptions.ValidationError(_('Please provide an ISBN for %s') % book.name)
            return True

