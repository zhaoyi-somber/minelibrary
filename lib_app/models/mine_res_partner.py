# -*- coding:utf-8 -*-

from odoo import fields, models


class MinePartner(models.Model):
    _inherit = 'res.partner'

    # 出版书籍
    published_book_ids = fields.One2many(comodel_name='mine.library.book', inverse_name='publisher_id', string='Published Books')
    # 书籍
    book_ids = fields.Many2many(comodel_name='mine.library.book', string='Authored Books')