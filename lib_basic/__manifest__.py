# -*- coding: utf-8 -*-
{
    'name': "Library Basic",

    'summary': """
    Library Basic：基础数据
    """,

    'description': """
        "管理用户权限：是否可以借书"
    """,

    'author': "somber",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['lib_app', 'mail'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/mine_lib_basic_info_member.xml',
        'views/mine_lib_basic_info_book.xml',
        'views/mine_lib_basic_info_publisher.xml',
        'views/mine_lib_basic_info_checkout.xml',
        'views/lib_basic_menus.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
    ],
}