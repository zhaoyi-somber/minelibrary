# -*- coding:utf-8 -*-

from odoo import _, models, fields, api, exceptions


class MineLibBasicInfoPublisher(models.Model):
    """
    基本数据 - 出版社基本信息
    """
    _name = 'mine.lib.basic.info.publisher'
    _description = 'Mine Lib Basic Info Publisher'
    _order = 'publisher_number'

    # 出版社编号
    publisher_number = fields.Char(string='Publisher Number', help='Publisher Number')
    # 出版社名
    name = fields.Char(string='Publisher Name', help='Publisher Name')
    # 出版社编码
    code = fields.Char(string='Publisher Code')
    # 出版国家
    publisher_country_id = fields.Many2one(comodel_name='res.country', string='Publisher Country', required=True)