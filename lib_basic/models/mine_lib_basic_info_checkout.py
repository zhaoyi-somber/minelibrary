# -*- coding:utf-8 -*-

from odoo import _, models, fields, api, exceptions


class MineLibBasicInfoCheckout(models.Model):
    """
    基本数据 - 借阅记录信息
    """
    _name = 'mine.lib.basic.info.checkout'
    _description = 'Mine Lib Basic Info Checkout'
    _order = 'checkout_number'

    # 借阅编号
    checkout_number = fields.Char(string='Checkout Number', help='Checkout Number')
