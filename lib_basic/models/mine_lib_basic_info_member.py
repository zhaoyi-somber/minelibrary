# -*- coding:utf-8 -*-

from odoo import _, models, fields, api, exceptions


class MineLibBasicInfoMember(models.Model):
    """
    基本数据 - 会员基本信息
    """
    _name = 'mine.lib.basic.info.member'
    _description = 'Mine Lib Basic Info Member'
    _order = 'member_number'

    # 会员编号
    member_number = fields.Char(string='Member Number', help='Member Number')
    # 会员名
    name = fields.Char(string='Member Name', help='Member Name')
    # 会员编码
    code = fields.Char(string='Member Code')
    # 卡号
    card_number = fields.Char(string='Card Number')
    # 会员
    member = fields.Char(required=True, string='Partner')
    # 邮箱
    email = fields.Char(string='Email')
    # 联系方式
    mobile_number = fields.Char(string='Mobile Number')