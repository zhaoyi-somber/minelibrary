# -*- coding:utf-8 -*-

from odoo import _, models, fields, api, exceptions


class MineLibBasicInfoBook(models.Model):
    """
    基本数据 - 图书基本信息
    """
    _name = 'mine.lib.basic.info.book'
    _description = 'Mine Lib Basic Info Book'
    _order = 'book_number'

    # 图书编号
    book_number = fields.Char(string='Book Number', help='Book Number')
    # 书名
    name = fields.Char(string='Book Name', help='Book Name')
    # 图书编码
    code = fields.Char(string='Book Code')
    # ISBN
    isbn = fields.Char(string='Book ISBN', help='Book ISBN')
    # 作者
    author = fields.Char(string='Authors', help='Authors')
    # 出版日期
    date_published = fields.Date(string='Date Published', help='Date Published')
    # 出版社
    publisher_id = fields.Many2one(comodel_name='mine.lib.basic.info.publisher', string='Publisher', help='Publisher')
    # 单价
    price = fields.Monetary(string='Price', currency_field='currency_id', digits=(3, 2))
    # 单位
    currency_id = fields.Many2one(comodel_name='res.currency', string='Currency')
    # 图书类型
    book_type = fields.Selection(
        [('paper', 'Paperback'), ('hard', 'Hardcover'), ('electronic', 'Electronic'), ('other', 'Other')],
        string='Type')
    # 数量
    copies = fields.Integer(default=1, string='Copies')
    # 评分
    avg_rating = fields.Float(string='Average Rating', digits=(3, 2))
    # 出版国家
    publisher_country_id = fields.Char(string='Publisher Country', rewuired=True)
    # 有效
    state = fields.Selection(string='State', help='State',
                             selection=[('active', 'Active'),
                                        ('invalid', 'Invalid')],
                             default='active')
    # 图片
    image = fields.Binary(string='Image', help='Image')
    # 内部说明
    notes = fields.Text(string='Internal Notes')
    # 说明
    descr = fields.Html(string='Description')
