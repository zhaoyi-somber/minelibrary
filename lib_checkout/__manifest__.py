# -*- coding: utf-8 -*-
{
    'name': "Library Checkout",

    'summary': """
    Library Checkout：图书借阅管理
    """,

    'description': """
        "为图书会员添加借书的功能"
    """,

    'author': "somber",
    'website': "http://www.yourcompany.com",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/12.0/odoo/addons/base/data/ir_module_category_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['lib_member', 'mail'],

    # always loaded
    'data': [
        'security/ir.model.access.csv',
        'views/mine_library_checkout.xml',
        'views/checkout_menus.xml',
        'wizard/mine_checkout_mass_message_wizard.xml',
        'data/mine_library_checkout_stage.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
    ],
}