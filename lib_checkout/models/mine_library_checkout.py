# -*- coding:utf-8 -*-

from odoo import fields, models, api, exceptions


class MineCheckout(models.Model):
    _name = 'mine.library.checkout'
    _description = 'Checkout Request'
    _inherit = ['mail.thread', 'mail.activity.mixin']
    _rec_name = 'member_id'

    # 会员
    member_id = fields.Many2one(comodel_name='mine.library.member', required=True)
    # 管理员
    user_id = fields.Many2one(comodel_name='res.users', string='Librarian', default=lambda s: s.env.uid)
    # 申请日期
    request_date = fields.Date(default=lambda s: fields.Date.today())
    # 借书信息
    line_ids = fields.One2many(comodel_name='mine.library.checkout.line', inverse_name='head_id', string='Borrowed Books')
    # 借阅日期
    checkout_date = fields.Date(readonly=True)
    # 结束日期
    close_date = fields.Date(readonly=True)

    @api.model
    def create(self, vals):
        if 'stage_id' in vals:
            # Code before create: should use the `vals` dict
            Stage = self.env['mine.library.checkout.stage']
            new_state = Stage.browse(vals['stage_id']).state
            if new_state == 'open':
                vals['checkout_date'] = fields.Date.today()
        new_record = super().create(vals)
        # Code after create: can use the `new_record` created
        if new_record.state == 'done':
            raise exceptions.UserError(
                'Not allowed to create a checkout in the done state.')
        return new_record

    @api.multi
    def write(self, vals):
        # Code before write: can use `self`, with the old values
        if 'stage_id' in vals:
            Stage = self.env['mine.library.checkout.stage']
            new_state = Stage.browse(vals['stage_id']).state
            if new_state == 'open' and self.state != 'open':
                vals['checkout_date'] = fields.Date.today()
            if new_state == 'done' and self.state != 'done':
                vals['close_date'] = fields.Date.today()
        super().write(vals)
        # Code after write:: can use `self`, with the updated values
        return True

    @api.model
    def _default_stage(self):
        Stage = self.env['mine.library.checkout.stage']
        return Stage.search([], limit=1)

    @api.model
    def _group_expand_stage_id(self, stages, domain, order):
        return stages.search([], order=order)

    # 头部状态
    stage_id = fields.Many2one(
        'mine.library.checkout.stage',
        default=_default_stage,
        group_expand='_group_expand_stage_id')
    # 状态
    state = fields.Selection(related='stage_id.state')
    # 会员邮件
    member_image = fields.Binary(related='member_id.partner_id.image')
    # 待归还
    num_other_checkouts = fields.Integer(compute='_compute_num_other_checkouts')
    # 借阅数量
    num_books = fields.Integer(compute='_compute_num_books', string='Num Books')

    @api.model
    def _compute_num_other_checkouts(self):
        for rec in self:
            domain = [
                ('member_id', '=', rec.member_id.id),
                ('state', 'in', ['open']),
                ('id', '!=', rec.id)]
        rec.num_other_checkouts = self.search_count(domain)

    @api.depends('line_ids')
    def _compute_num_books(self):
        for book in self:
            book.num_books = len(book.line_ids)

    @api.multi
    def button_done(self):
        Stage = self.env['mine.library.checkout.stage']
        done_stage = Stage.search(
            [('state', '=', 'done')],
            limit=1)
        for checkout in self:
            checkout.stage_id = done_stage
        return True


class MineCheckoutLine(models.Model):
    _name = 'mine.library.checkout.line'
    _description = 'Borrow Request Line'

    # 头id
    head_id = fields.Many2one(comodel_name='mine.library.checkout')
    # 会员
    member_id = fields.Many2one(string='Member', comodel_name='mine.library.member', required=True)
    # 图书
    book_id = fields.Many2one(string='Book', comodel_name='mine.library.book', required=True)
