# -*- coding:utf-8 -*-

from odoo import fields, models


class MineCheckoutStage(models.Model):
    _name = 'mine.library.checkout.stage'
    _description = 'Checkout Stage'
    _order = 'sequence,name'

    name = fields.Char()
    sequence = fields.Integer(default=10)
    fold = fields.Boolean()
    active = fields.Boolean(default=True)
    state = fields.Selection([('new', 'New'), ('open', 'Borrowed'), ('done', 'Returned'), ('cancel', 'Cancelled')], default='new')