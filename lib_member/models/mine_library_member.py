# -*- coding:utf-8 -*-

from odoo import fields, models


class MineLibraryMember(models.Model):
    _name = 'mine.library.member'
    _description = 'Mine Library Member'
    _inherit = ['mail.thread', 'mail.activity.mixin']

    # 会员名
    name = fields.Char(string='Member Name')
    # 卡号
    card_number = fields.Char()
    # 会员
    partner_id = fields.Many2one(comodel_name='res.partner', delegate=True, ondelete='cascade', required=True, string='Partner')