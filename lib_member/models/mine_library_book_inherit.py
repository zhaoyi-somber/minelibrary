# -*- coding:utf-8 -*-
from odoo import fields, models, api


class MineLibraryBookInherit(models.Model):
    _inherit = 'mine.library.book'

    # 是否可借
    is_available = fields.Boolean('Is Available')
    # ISBN
    isbn = fields.Char(help="Use a valid ISBN-13 or ISBN-10.")
    # 出版公司
    publisher_id = fields.Many2one(index=True)

    @api.multi
    def _check_isbn(self):
        isbn_length = 10
        isbn = self.isbn.replace('-', '')
        digits = [int(x) for x in isbn if x.isdigit()]
        if len(digits) == isbn_length:
            ponderators = [1, 2, 3, 4, 5, 6, 7, 8, 9]
            total = sum(a * b for a, b in zip(digits[:9], ponderators))
            check = total % 11
            return digits[-1] == check
        else:
            return super()._check_isbn()
