# -*- coding:utf-8 -*-
from odoo import http
from minelibrary.develop.minelibrary.lib_app.controllers.main import Books


class BookExtended(Books):
    """
    继承修改：lib_app.Books
    显示有效图书列表的简单网页
    访问地址：192.168.99.100:8069/mine/library/books
    """
    @http.route()
    def list(self, **kwargs):
        response = super().list(**kwargs)
        if kwargs.get('available'):
            Book = http.request.env['mine.library.book']
            books = Book.search([('is_available', '=', True)])
            response.qcontext['books'] = books
        return response